# mosquitto-helm

Helm chart to install Mosquitto server on Kubernetes

## Installation

```bash
helm upgrade -i mqtt mosquitto -n mqtt --create-namespace
 
helm upgrade -i mqtt mosquitto -n mqtt --create-namespace --set volumes.data.storageClassName="local-path" --set volumes.log.storageClassName="local-path"
helm upgrade -i mqtt mosquitto -n mqtt --create-namespace --set volumes.data.storageClassName="synology-csi-delete" --set volumes.log.storageClassName="synology-csi-delete"
```
> Not use service port 1883, it is internally hardcoded in the container. 

## Access container

```bash
kubectl exec -n mqtt -it $(kubectl get po -n mqtt -l"app.kubernetes.io/instance"=mqtt -o name) -- /bin/sh
```

## Deinstallation

```bash
helm delete mqtt -n mqtt
```

## Generate passwords for users

```bash
kubectl exec -it $(kubectl get po -l"app.kubernetes.io/instance"=mqtt -o name) -- rm /tmp/passfile
kubectl exec -it $(kubectl get po -l"app.kubernetes.io/instance"=mqtt -o name) -- touch /tmp/passfile
kubectl exec -it $(kubectl get po -l"app.kubernetes.io/instance"=mqtt -o name) -- mosquitto_passwd -b /tmp/passfile admin "xpto"
kubectl exec -it $(kubectl get po -l"app.kubernetes.io/instance"=mqtt -o name) -- mosquitto_passwd -b /tmp/passfile user  "xpto"
kubectl exec -it $(kubectl get po -l"app.kubernetes.io/instance"=mqtt -o name) -- mosquitto_passwd -b /tmp/passfile app-server  "xpto"
kubectl exec -it $(kubectl get po -l"app.kubernetes.io/instance"=mqtt -o name) -- mosquitto_passwd -b /tmp/passfile net-server  "xpto"
kubectl exec -it $(kubectl get po -l"app.kubernetes.io/instance"=mqtt -o name) -- mosquitto_passwd -b /tmp/passfile gateway-bridge  "xpto"
kubectl exec -it $(kubectl get po -l"app.kubernetes.io/instance"=mqtt -o name) -- cat /tmp/passfile
```


The file should look like the example below
```
admin:$7$101$62zcpIkszkBceiTN$fGzx957xb8UX+2pzbmTL6Zvpzl+gMwTWTj7nl9oeWjKHoABY262pZmJ6xl4V+E+OPqeea9+Bu3qcNs76n3SwgA==
user:$7$101$uFJcW/J+WOSUEJb1$wxcIMWAN7V8ImsBvO/yCuBgZM4nrDVP6I6YoH2Aw3/O1K4xSOYZfEGY7ctSrmU2iPssA4mNppndtddhydc0U6Q==
app-server:$7$101$VdLrgIe7TFRmJdbL$VfrtqJfzY5nRBrcbqlVkZTCD6SW0xy1gtTvWq4/KvaQF74gj/OTFJ2dCDUUpCg0pc30SqwZp7FOGY6KmDp7oqA==
net-server:$7$101$XFlAAzm9nDoptVEI$SYLGoh991u8PB5nYroOytN1uuY5n0Thau04dVGm77yTMzRI3uvQr2G+tXfrlliT+kUQFmC0LOCJyLasJUs422g==
gateway-bridge:$7$101$nIwyC/rnTkPkMyRx$OBHVBVoR1jquSd1MOFw8OUgSUesNiSQldxTWzrdjTOlLD1dYl5FOJZoPJsSxaZEw0mRKorJcm3wkR/A0+f2ZWg==
```

Add the entries to the users list in the values for the chart.

## Roadmap

- [ ] Add support to generate passwords for users
- [ ] Add support to configure TLS using Kubernetes secrets
- [ ] Extend configuration options
- [ ] Disable 1883 when listening to other port than 1883